--- 
title: Maandag  
date: 2017-12-04
---

# Dag 6

We hebben laatste stukjes voor de pitch voorbereid en ook heeft Sico de pitch geschreven en geoefend. Het validatie moment voor de prototype was ook die dag, ik heb een prototype gemaakt en laten valideren. De prototype was nog niet testbaar genoeg en had nog een paar aanpassingen nodig. Hiervoor kan ik volgende week terug komen om opnieuw te laten valideren.