--- 
title: Maandag  
date: 2017-11-27 
---

# Dag 4

We zijn de dag begonnen met een stand-up, en hebben een week planning op gehangen met post-its. We zijn begonnen met het maken van een user journey, door de enquête die we hadden afgenomen weten we nu beter wat mensen zouden denken op bepaalde momenten en de redenen waarom ze niet vaak/vaker naar paard gaan. Dit hebben we ook in onze user journey gebruikt.

Het 2e onderdeel waar we als groep aan hebben gewerkt is de mindmap. We hebben onze eerder gemaakte mindmap onderverdeelt in 4 su categorieën en allemaal 1 onderwerp gekozen en daar een eigen uitgebreide mindmap van gemaakt daarna zijn we gaan vergelijken en de belangrijkste steekwoorden uit elke mindmap gehaald om die in een relevant/niet relevant rijtje te kunnen zetten.

Hierdoor zijn we tot bepaalde conclusies gekomen, mensen kennen het paard niet en zijn ook niet onder de indruk als ze voor het eerst de website van het paard zien, te chaotisch en niet gelijk genres/ artiesten die hun aanspreken. Mensen die het paard wel kennen zijn bereid om verder te reizen maar hebben ook moeite met de genre's en artiesten. Dit word voor ons het belangrijkste verbeter punt.

De rest van de dag ben ik verder gegaan met het aanpassen van mn merkanalyse zodat het Woensdag af is.