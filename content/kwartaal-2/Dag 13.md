--- 
title: Maandag  
date: 2018-01-15
---


# Dag 13

We hebben besproken wat we gaan doen voor de expo. We gaan  de volgende dingen laten zien:

- concept poster
- handout (enveloppen met paard kaart)
- scenario beschrijving
- prototype
- pitch

Ik heb het test plan afgemaakt laten checken door mensen uit mn team en feedback gevraagd aan een docent. Aanpassingen gemaakt zodat het morgen kan worden gevalideerd.

Samen met Lotte ben ik begonnen met het zoeken naar voorbeelden en materiaal voor het maken van enveloppen. Jordan zou beginnen met het maken van een concept poster.