--- 
title: Woensdag  
date: 2018-01-17
---


# Dag 14

Gister had ik het prototype en het testplan van het prototype laten valideren. Vandaag moesten we alleen nog kleinen dingetjes af te maken voor de expo. Jordan heeft de laatste dingen aan de poster afgemaakt. Sico was bezig met het voorbereiden van de pitch, ik was bezig met het maken van de enveloppen die we bij de expo zouden neerleggen.

S'avonds was de expo, we hadden alles op de tafel klaar gezet en de poster en ons groep logo op gehangen. Paard/Leeraren/Fabrique  waren erg positief over ons concept en de uitwerking daarvan. Ook veel leerlingen waren onder de indruk van de uitwerking. 