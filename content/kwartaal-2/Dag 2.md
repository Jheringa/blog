--- 
title: Maandag  
date: 2017-11-20 
---

# Dag 2

We zijn de dag begonnen met het afmaken en vragen om feedback van de debriefing en merkanalyse, verbeteringen doorvoeren en kijken wat we voor de rest van de week nog moesten maken.

Aan het eind van de dag hebben we als team een workshop gevolgd over het maken van team afspraken, een swot analyse maken en daarbij onze sterkte en zwakte punten als persoon en als team op te schrijven.