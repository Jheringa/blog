--- 
title: Donderdag - 3
date: 2017-09-07 
---

## Dag 3

* Paper prototype
* Concept

3e project dag. We hebben het concept weer een beetje aangepast. Onze vraag was hoe zorgen we dat studenten het spel afmaken, hoe blijft het spel leuk, wat is het doel? 
Begin gemaakt aan onze paper prototype, dmv het maken van wireframes en zo duidelijk gemaakt welke features en schermen we nodig hebben.