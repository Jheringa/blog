--- 
title: Woensdag - 5
date: 2017-09-13 
---

## Dag 5

* paper prototype
* Workshop style creeren

Uit tekenen van de paper prototype, presenteren van onze paper prototype. De feedback van onze prototype was positief, duidelijk uitgelegd, goeie paper prototype, goed aangegeven van de vaste menu balk aan de onderkant. Beter kon ipv potloot met fineliner overtrekken. Middags heb ik een workshop style creeren gevolgd, dit ging over de branding van bedrijven en hoe handig het is om te hebben als een team. Voor mij was het vooral bekende informatie omdat ik dit ook vaak op mn vorige opleiding heb moeten doen. In de les heb ik een style guide gemaakt voor ons project.