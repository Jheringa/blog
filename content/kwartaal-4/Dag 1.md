--- 
title: Maandag  
date: 2018-04-09 
---

### Dag-1

Vandaag was de kick-off van deel twee van het project.
We kregen een korte recap van wat we vorige periode hadden moeten berijken en uitloeg van Joke over welke gebieden we konden kiezen om te onderzoeken.
We hebben de zelfde teams. We zijn begonnen met het kiezen van een gebied. We hebben gekozen voor Afrikanenwijk omdat ons onderzoek vooral naar voeding gaat.
Dit gebied is vooral gefocused op voeding wat het hopelijk makkelijker maakt en we dieper onderzoek kunnen gaan doen.

Vandaag zijn we ook begonnen met het maken van een takenlijst, verdelen van de competenties en een scrumboard.
Mijn taak is om voor woensdag deskresearch te doen naar de wijk en de mensen en daar een (onderzoeks) persona van te maken.