---
title: Evaluatie S4  
date: 2018-06-03 
---


## Evaluatie S1 - S4

Dit is de laatste post, terug kijkend op afgelopen 4 periodes. Ik vond het een erg leuk en leerzaam schooljaar.
Periode 4 was voor mij een rustige periode, doordat ik al 4 competenties had gehaald en duidelijk voormezelf voor ogen had hoe ik de overige competenties wou gaan halen.
Mijn grootste insteek was deze periode ook andere helpen op de punten die nodig was. Feedback geven en helpen met prototypen.

Overal vond ik het werken in teams en het leren van anderen was erg fijn en ben ook blij met de teams waarin ik heb gezeten.
Het hoogte punt was het presenteren bij paard. Het was eer om dat mee te mogen maken.

Volgend jaar me verder verdiepen in het onderzoeken en het leren van nieuwe technieken. 
Hopelijk zijn ook wat meer design gerichten workshops 

Groetjes!


