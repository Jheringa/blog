--- 
title: Woensdag  
date: 2018-04-25 
---

## Dag 6

We hebben als team het interview en observatie voorbereid die later die dag zou worden uitgevoerd. Helaas kon ik zelf niet mee omdat ik een workshop deskresearch moest volgen en het interview echt op die dag moest worden uitgevoerd om niet achter te lopen in het project.
We hebben na aanleiding van de feedback van bob de kaartjes een klein beetje aangepast die we zouden uitdelen aan de kinderen op de school.

Smiddags heb ik deel 2 van de workshop deskresearch gevolgd.