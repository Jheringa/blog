---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---


Mijn naam is Jiska Heringa, ik ben 22 jaar en  sinds kort afgestudeert in Interactive media design. 
Gespecialiseerd in het designen en bouwen van websites. Ik ben erg leergierig en altijd opzoek naar nieuwe dingen om te leren. 
In mn vrije tijd ben ik te vinden waar dr golven zijn en ben ik vaak bezig met het maken van muziek.

### my history

*-2013/ 2017*
**Grafisch Lyceum Rotterdam**

- Interactive media design (English)


*2016 Jan - October* 
**Crowdcomms Australia**

- Designen van de app skin
- Vullen van de app content
- Comunicatie met de klant
 (App problemen oplossen)

