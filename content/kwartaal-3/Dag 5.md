--- 
title: Maandag  
date: 2018-03-05 
---

### Dag 5


We zijn de dag begonnen met allemaal werken aan onze lifestyle diary. De teamleiders hadden een stand-up die we daarna als groep hebben besproken. We zijn er achter gekomen dat we best achter liggen met het project en we nu echt moeten beginnen met het onderzoeken en interviewen. Samen met Floris heb ik interview vragen opgesteld. We willen de doelgroep testen of ze weten hoeveel calorieen er in bepaalde voeding / drank zit en of ze weten hoeveel calorieen je verbrand met dagelijkse bezigheden. Dit gaan we doen door middel van kaartjes. Op de voorkant van het kaartje is een plaatje te zien en de geinterviewde moet raden hoeveel calorieen hij/zij verbrand met deze bezigheden, op de achterkant van het kaartje staat het antwoord. Floris gaat de voeding kaartjes maken en ik maak de klusjes kaartjes. Woensdag willen wij dit testen op verschillende CMD-studenten.